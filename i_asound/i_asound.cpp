#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include "blc_channel.h"
#include "blc_program.h"

#define DEFAULT_OUTPUT_NAME ":sound<pid>"
#define EXIT_ON_SOUND_ERROR(err, msg, ...) EXIT_ON_ERROR("ALSA:%s\n\t" msg, snd_strerror(err), ##__VA_ARGS__)

snd_pcm_t *init_sound(char const *device_name, unsigned int sample_rate)
{
   int result, dir = 0;
   snd_pcm_hw_params_t *hw_params = NULL;
   snd_pcm_t *capture_handle = NULL;

   result = snd_pcm_open(&capture_handle, device_name, SND_PCM_STREAM_CAPTURE, 0);
   if (result < 0) {
	   if (system("arecord -l >2")==-1)  PRINT_WARNING("Impossible to get the list of input sound device");
	   EXIT_ON_SOUND_ERROR(result, "Cannot open audio device '%s'. See the list of devices above\n\t", device_name);
   }
   result = snd_pcm_hw_params_malloc(&hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot allocate hardware parameter structure");
   result = snd_pcm_hw_params_any(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot initialize hardware parameter structure");
   result = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set access type");
   result = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample format");
   result = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &sample_rate, &dir);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample rate");
   result = snd_pcm_hw_params_set_channels(capture_handle, hw_params, 2);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set channel count");
   result = snd_pcm_hw_params(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set parameters");
   snd_pcm_hw_params_free(hw_params);
   result = snd_pcm_prepare(capture_handle);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot prepare audio interface for use");
   return capture_handle;
}

int main(int argc, char **argv)
{
	blc_channel output;
    char const *frames_nb_str, *type_str, *output_name, *device_name, *samplerate_str, *iterations_nb_str;
    snd_pcm_t *capture_handle = NULL;
    int16_t *buffer;
    uint samplerate;
    int i, iterations_nb;

    int frames_nb;
    uint32_t type;
    int result;
    
    blc_program_set_description("Acquire the audio buffer on a blc_channel.");
   // blc_program_add_option(&display, 'd', "display", "F|WIDTHxHEIGHT", "display a text graph. 'F' for fullscreen. -1 for width or heigt for max dimention.", NULL);
    blc_program_add_option(&device_name, 'D', "device", "string", "device to use for the sound", "default");
    blc_program_add_option(&samplerate_str, 'f', "samplerate", "integer", "frequency of sampling", "44100");
    blc_program_add_option(&iterations_nb_str, 'n', "number", "integer", "number of samples to acquire (-1 for infinity)", "-1");
    blc_program_add_option(&output_name, 'o', "output", "string", "name of the channel where the buffer will be put", DEFAULT_OUTPUT_NAME);
    blc_program_add_option(&frames_nb_str, 's', "size", "integer", "size of the sound buffer", "4096");
    blc_program_add_option(&type_str, 't', "type", "INT8|FL32", "type of the data", "FL32");
    blc_program_init(&argc, &argv, NULL);
    
    SSCANF(1, samplerate_str, "%d", &samplerate);
    SSCANF(1, frames_nb_str, "%d", &frames_nb);
    type=STRING_TO_UINT32(type_str);
    iterations_nb=strtol(iterations_nb_str, NULL, 10);

    
    if (strcmp(DEFAULT_OUTPUT_NAME, output_name)==0) {
        SYSTEM_ERROR_CHECK(asprintf((char**)&output_name, ":sound%d", getpid()),-1, NULL);
    }
    
    capture_handle = init_sound(device_name, samplerate);

    if (type=='FL32') buffer=MANY_ALLOCATIONS(frames_nb*2*sizeof(int16_t), int16_t);
    else EXIT_ON_ERROR("Only FL32 is managed");

    output.create_or_open(output_name, BLC_CHANNEL_WRITE, type, 'LPCM', 1, frames_nb);
    output.publish();
    
    blc_loop_try_add_waiting_semaphore(output.sem_ack_data);
    blc_loop_try_add_posting_semaphore(output.sem_new_data);

    BLC_COMMAND_LOOP(0){
	if (iterations_nb==blc_loop_iteration) blc_command_ask_quit();
    	if ((result = snd_pcm_readi(capture_handle, (char*)buffer, frames_nb)) != frames_nb)
    	{
    		switch(result){
    			case -EPIPE:color_eprintf(BLC_RED, "EPIPE: an overrun occurred, you are not fast enough to read data\n");
    				break;
    			case -ESTRPIPE:color_eprintf(BLC_RED, "ESTRPIPE: a suspend event occurred (stream is suspended and waiting for an application recovery)\n");
    			   break;
    			case -EBADFD:color_eprintf(BLC_RED, "EBADFD: PCM is not in the right state (SND_PCM_STATE_PREPARED or SND_PCM_STATE_RUNNING)\n");
    			   break;
    		}
			EXIT_ON_SOUND_ERROR(result, "read from audio interface failed '%s'\n", device_name);
    	}
    	FOR(i, frames_nb){
    		output.floats[i]=buffer[i * 2]/ (float)(1 << 15);
    	}
    }
    snd_pcm_close(capture_handle);

    
    return EXIT_SUCCESS;
}

