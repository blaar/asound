cmake_minimum_required(VERSION 2.6)

project(i_asound)

find_package(blc_channel REQUIRED)
find_package(blc_program REQUIRED)
find_library(ASOUND_LIBRARY asound)

add_definitions(${BL_DEFINITIONS})
include_directories(${BL_INCLUDE_DIRS})
add_executable(i_asound i_asound.cpp)
target_link_libraries(i_asound ${BL_LIBRARIES} ${ASOUND_LIBRARY})
  
