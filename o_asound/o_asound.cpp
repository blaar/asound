#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <math.h>
#include "blc_channel.h"
#include "blc_program.h"


#define EXIT_ON_SOUND_ERROR(err, msg, ...) EXIT_ON_ERROR("ALSA:%s\n\t" msg, snd_strerror(err), ##__VA_ARGS__)

snd_pcm_t *init_sound(char const *device_name, unsigned int sample_rate)
{
   int result, dir = 0;
   snd_pcm_hw_params_t *hw_params = NULL;
   snd_pcm_t *capture_handle = NULL;

   result = snd_pcm_open(&capture_handle, device_name, SND_PCM_STREAM_PLAYBACK, 0);
   if (result < 0) {
	   if (system("aplay -l")==-1) PRINT_WARNING("Impossible to get the list of input sound device");
	   EXIT_ON_SOUND_ERROR(result, "Cannot open audio device '%s'. See the list of devices above\n\t", device_name);
   }
   result = snd_pcm_hw_params_malloc(&hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot allocate hardware parameter structure");
   result = snd_pcm_hw_params_any(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot initialize hardware parameter structure");
   result = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set access type");
   result = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample format");
   result = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &sample_rate, &dir);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set sample rate");
   result = snd_pcm_hw_params_set_channels(capture_handle, hw_params, 2);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set channel count");
   result = snd_pcm_hw_params(capture_handle, hw_params);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot set parameters");
   snd_pcm_hw_params_free(hw_params);
   result = snd_pcm_prepare(capture_handle);
   if (result < 0) EXIT_ON_SOUND_ERROR(result, "cannot prepare audio interface for use");
   return capture_handle;
}


int main(int argc, char **argv){
	blc_channel input;
    char const  *input_name, *device_name, *samplerate_str;
    snd_pcm_t *capture_handle = NULL;
    int16_t *buffer;
    uint samplerate;
    int i, frames_nb, result;
    
    blc_program_set_description("Play the input blc_channel on speaker.");
    blc_program_add_option(&samplerate_str, 'f', "samplerate", "integer", "frequency of sampling", "44100");
    blc_program_add_option(&device_name, 'D', "device", "string", "device to use for the sound", "default");
    blc_program_add_parameter(&input_name, "blc_channel-in", 1, "channel with the input data", NULL);
    blc_program_init(&argc, &argv, NULL);
    
    SSCANF(1, samplerate_str, "%d", &samplerate);

    capture_handle = init_sound(device_name, samplerate);

    input.open(input_name, BLC_CHANNEL_READ);

    frames_nb=input.dims[0].length;

    if (input.type=='FL32') buffer=MANY_ALLOCATIONS(frames_nb*2*sizeof(int16_t), int16_t);
    else EXIT_ON_ERROR("Only FL32 is managed");
    
    blc_loop_try_add_waiting_semaphore(input.sem_new_data);
    blc_loop_try_add_posting_semaphore(input.sem_ack_data);

    BLC_COMMAND_LOOP(0){
    	FOR(i, frames_nb){
    				buffer[i * 2]=input.floats[i]*(1 << 15);
    	}

    	if ((result = snd_pcm_writei(capture_handle, (char*)buffer, frames_nb)) != frames_nb)
    	{
    		switch(result){
    			case -EPIPE:color_eprintf(BLC_RED, "EPIPE: an overrun occurred, you are not writing data fast enough\n");
    				break;
    			case -ESTRPIPE:color_eprintf(BLC_RED, "ESTRPIPE: a suspend event occurred (stream is suspended and waiting for an application recovery)\n");
    			   break;
    			case -EBADFD:color_eprintf(BLC_RED, "EBADFD: PCM is not in the right state (SND_PCM_STATE_PREPARED or SND_PCM_STATE_RUNNING)\n");
    			   break;
    		}
			EXIT_ON_SOUND_ERROR(result, "write to audio interface failed '%s'\n", device_name);
    	}
    }
    snd_pcm_close(capture_handle);

    
    return EXIT_SUCCESS;
}

